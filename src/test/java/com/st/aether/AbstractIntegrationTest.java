package com.st.aether;

import javax.sql.DataSource;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebIntegrationTest
@SpringApplicationConfiguration(classes=AetherApplication.class)
public abstract class AbstractIntegrationTest {

    protected MockMvc mvc;

    @Autowired
    protected WebApplicationContext wac;

    protected static JdbcTemplate jdbcTmp;
    
    protected static final Logger LOG = LoggerFactory.getLogger(AbstractIntegrationTest.class);

    @Before
    public void setUpOnce() throws Exception {
        if (null == mvc) {
            mvc = MockMvcBuilders.webAppContextSetup(wac).build();
            jdbcTmp = new JdbcTemplate(wac.getBean(DataSource.class));
        }
    }

}