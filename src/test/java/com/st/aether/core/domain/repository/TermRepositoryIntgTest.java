package com.st.aether.core.domain.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.st.aether.AbstractIntegrationTest;
import com.st.aether.core.domain.Term;

@RunWith(SpringJUnit4ClassRunner.class)
public class TermRepositoryIntgTest extends AbstractIntegrationTest {

	@Autowired 
	private TermRepository termRepository;
	
	@Test
	public void canListTems(){
		List<Term> terms = termRepository.findAll();
		assertThat(terms, notNullValue());
		assertThat(terms.size(), greaterThanOrEqualTo(2));
	}
	
	@Test
	public void canFindOneTerm(){
		int term2Id = 2;
		
		Term term = termRepository.findOne(term2Id);
		assertThat(term.getName(), equalTo("term2"));
		assertThat(term.getId(), equalTo(term2Id));
	}
}
