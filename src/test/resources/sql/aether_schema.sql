CREATE DATABASE  IF NOT EXISTS `aether`;

DROP TABLE IF EXISTS  `aether`.`Term`;
CREATE TABLE `aether`.`Term` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `description` varchar(255) NULL,
  `state` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_key` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;