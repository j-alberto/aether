INSERT INTO `minos`.`user` (`id`,`username`, `password`, `enabled`) VALUES (1,'admin', 'admin', 1);
INSERT INTO `minos`.`user` (`id`,`username`, `password`, `enabled`) VALUES (2,'user', 'user', 1);
INSERT INTO `minos`.`user` (`id`,`username`, `password`, `enabled`) VALUES (3,'user2', 'user2', 0);
INSERT INTO `minos`.`user` (`id`,`username`, `password`, `enabled`) VALUES (4,'guest', 'guest', 1);

INSERT INTO `minos`.`group` (`id`, `name`) VALUES ('11', 'group1');
INSERT INTO `minos`.`group` (`id`, `name`) VALUES ('22', 'group2');
INSERT INTO `minos`.`group` (`id`, `name`) VALUES ('33', 'group3');

INSERT INTO `minos`.`authority` (`id`, `name`) VALUES ('1', 'CONFIG');
INSERT INTO `minos`.`authority` (`id`, `name`) VALUES ('2', 'WRITE');
INSERT INTO `minos`.`authority` (`id`, `name`) VALUES ('3', 'READ');

INSERT INTO `minos`.`group_user` (`id`, `group_id`, `user_id`) VALUES ('1', '11', '1');
INSERT INTO `minos`.`group_user` (`id`, `group_id`, `user_id`) VALUES ('2', '22', '2');
INSERT INTO `minos`.`group_user` (`id`, `group_id`, `user_id`) VALUES ('3', '22', '3');
INSERT INTO `minos`.`group_user` (`id`, `group_id`, `user_id`) VALUES ('4', '33', '4');

INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('1', '1', '11');
INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('2', '2', '11');
INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('3', '3', '11');
INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('4', '2', '22');
INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('5', '3', '22');
INSERT INTO `minos`.`group_authority` (`id`, `authority_id`, `group_id`) VALUES ('6', '3', '33');
