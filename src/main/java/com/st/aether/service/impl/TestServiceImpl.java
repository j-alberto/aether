package com.st.aether.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.st.aether.core.domain.repository.TermRepository;
import com.st.aether.service.TestService;

@Service
public final class TestServiceImpl implements TestService {

	@Autowired
	private TermRepository termRepository;
	
	@Override
	@PreAuthorize("hasAuthority('CONFIG')")
	public String getString() {
		return "<br>"+termRepository.findOne(1).getName();
	}
	
	@Override
	@PreAuthorize("hasAuthority('READ')")
	public String getString2() {
		return "<br>"+termRepository.findOne(1).getName();
	}

}
