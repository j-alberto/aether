package com.st.aether.web.resource.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.st.aether.core.domain.Term;
import com.st.aether.core.domain.repository.TermRepository;
import com.st.aether.service.TestService;
import com.st.aether.web.resource.TermResource;

@RestController
@RequestMapping(value="/v1/term", produces="application/json; charset=UTF-8")
public class TermResourceImpl implements TermResource {

	private TermRepository termRepository;
	@Autowired 
	private TestService testService;
	
	@Autowired
	public TermResourceImpl(TermRepository termRepository) {
		this.termRepository = termRepository;
	}

	@Override
	@RequestMapping(method=RequestMethod.GET)
	public List<Term> getTerms() {		
		return termRepository.findAll();
	}

	@Override
	@RequestMapping(path="/2", method=RequestMethod.GET)
	public List<Term> getTerms2() {
		testService.getString();
		return termRepository.findAll();
	}

	@Override
	@RequestMapping(path="/3", method=RequestMethod.GET)
	public List<Term> getTerms3() {		
		testService.getString2();
		return termRepository.findAll();
	}

}
