package com.st.aether.web.resource.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.st.aether.service.TestService;
import com.st.aether.web.resource.SecurityExample;

@RestController
@RequestMapping("/secure")
public final class SecurityExampleImpl implements SecurityExample {

	@Autowired
	private TestService testService;
	@Override
	@RequestMapping(path="/admin", method=RequestMethod.GET)
	public String method1() {
		return "admin can see this"+getAuths()+testService.getString();
	}

	@Override
	@RequestMapping(path="/config", method=RequestMethod.GET)
	public String method2() {
		return "config can see this"+getAuths();
	}

	@Override
	@RequestMapping(path="/write", method=RequestMethod.GET)
	public String method3() {
		return "write can see this"+getAuths();
	}

	@Override
	@RequestMapping(path="/read", method=RequestMethod.GET)
	public String method4() {
		return "read can see this"+getAuths() + testService.getString2();
	}

	@Override
	@RequestMapping(path="/special", method=RequestMethod.GET)
	public String method5() {
		return "admin config can see this"+getAuths();
	}

	@Override
	@RequestMapping(path="/all", method=RequestMethod.GET)
	public String method6() {
		return "all can see this"+getAuths();
	}

	private String getAuths(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
		User user = (User)auth.getPrincipal();
		
		final StringBuilder auths = new StringBuilder();
		auth.getAuthorities().forEach((action) -> auths.append(action.getAuthority()).append(","));
		return String.format("<br>  Name: %s. Auths: %s. Details: %s. Principal: %s",
				auth.getName(),
				auths.toString(),
				details.getRemoteAddress()+" : "+details.getSessionId(),
				user.getUsername());
		
	}
}
