package com.st.aether.web.resource;

public interface SecurityExample {

	public String  method1();
	public String  method2();
	public String  method3();
	public String  method4();
	public String  method5();
	public String  method6();

}
