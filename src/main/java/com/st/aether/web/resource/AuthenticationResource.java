package com.st.aether.web.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

public interface AuthenticationResource {

	ResponseEntity<Authentication> openSession(String principal, String credentials);
	ResponseEntity<String> closeSession();
	
}
