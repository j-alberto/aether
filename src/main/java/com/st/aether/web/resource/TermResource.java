package com.st.aether.web.resource;

import java.util.List;

import com.st.aether.core.domain.Term;

public interface TermResource {

	public List<Term>  getTerms();
	public List<Term>  getTerms2();
	public List<Term>  getTerms3();

}
