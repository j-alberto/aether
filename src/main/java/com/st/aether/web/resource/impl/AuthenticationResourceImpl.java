package com.st.aether.web.resource.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.st.aether.web.resource.AuthenticationResource;

@RestController
@RequestMapping(path="/api", method=RequestMethod.POST)
public class AuthenticationResourceImpl implements AuthenticationResource {

	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationResourceImpl.class);
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
	@RequestMapping("/auth")
	public ResponseEntity<Authentication> openSession(
			@RequestParam("principal") String principal,
			@RequestParam("credentials") String credentials) {
		
		LOG.info("Principal: {}, Credentials: {}", principal, credentials);
		
		Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(principal, credentials));
		
		return new ResponseEntity<>(auth, HttpStatus.OK);
	}

	@Override
	@RequestMapping("close")
	public ResponseEntity<String> closeSession() {
		throw new RuntimeException("not implemented Yet");
	}

}
