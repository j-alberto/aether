package com.st.aether;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AetherApplication {

	public static void main(String[] args) {
		 ApplicationContext ctx = SpringApplication.run(AetherApplication.class, args);
		 for (String string : ctx.getBeanDefinitionNames()) {
			if(string.contains("uth") || string.contains("ecurity")){
				System.out.println(">>>>>>>>>>>"+string);
			}
		}
	}
}
