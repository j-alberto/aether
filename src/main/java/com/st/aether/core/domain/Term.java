package com.st.aether.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table
@NamedQuery(name="Term.findAll", query="SELECT t FROM Term t")
public class Term {

	@Id
	@GeneratedValue
	private int id;
	@Column(nullable=false, length=25)
	private String name;
	private String description;

	@Column(nullable=false, length=25)
	@Enumerated
	private State state;
	
	public final int getId() {
		return id;
	}
	public final void setId(int id) {
		this.id = id;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}
	public final String getDescription() {
		return description;
	}
	public final void setDescription(String description) {
		this.description = description;
	}
	public final State getState() {
		return state;
	}
	public final void setState(State state) {
		this.state = state;
	}
}
