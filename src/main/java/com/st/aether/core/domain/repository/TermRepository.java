package com.st.aether.core.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.st.aether.core.domain.Term;

@Repository
public interface TermRepository extends JpaRepository<Term, Integer> {

}
