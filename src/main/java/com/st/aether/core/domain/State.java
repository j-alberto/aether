package com.st.aether.core.domain;

/**
 * @author zero
 * Represents the states in the persistence model, hence limiting the actions allowed in it
 * IMPORTANT: do not remove or reorder this enum, since its an ordinal value in the DB
 */
public enum State {
	NEW,ENABLED, BLOCKED, DISABLED, DELETED;
}
