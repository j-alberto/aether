package com.st.aether;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private DataSource datasource;
	
	private static final String USER_BY_USERNAME = "select username,password,enabled from minos.user "
			+ "where username = ?";
	private static final String AUTH_BY_USERNAME = "select distinct u.username, a.name from minos.user u "
			+ "inner join minos.group_user gu on u.id = gu.user_id "
			+ "inner join minos.group_authority ga on gu.group_id = ga.group_id "
			+ "inner join minos.authority a on a.id = ga.authority_id "
			+ "where u.username = ?";
	private static final String GROUP_AUTH_BY_USERNAME = "select g.id, g.name, a.name from minos.user u "
			+ "inner join minos.group_user gu on u.id = gu.user_id "
			+ "inner join minos.group g on g.id = gu.group_id "
			+ "inner join minos.group_authority ga on gu.group_id = ga.group_id "
			+ "inner join minos.authority a on a.id = ga.authority_id "
			+ "where u.username = ?";
	
	private static final String API_ANT_MATCHER = "/v1/**";
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.jdbcAuthentication()
				.dataSource(datasource)
				.usersByUsernameQuery(USER_BY_USERNAME)
				.authoritiesByUsernameQuery(AUTH_BY_USERNAME)
				.groupAuthoritiesByUsername(GROUP_AUTH_BY_USERNAME)
			.and().inMemoryAuthentication()
				.withUser("root")
				.password("root")
				.authorities("ADMIN","CONFIG","READ","WRITE");
	}
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
        http.
        csrf()
        	.disable()
//        	.ignoringAntMatchers("/api/**")
//        	.headers()
//        .and()
        .exceptionHandling()
        	.defaultAuthenticationEntryPointFor(
        			new ApiEntryPoint(),
        			new AntPathRequestMatcher(API_ANT_MATCHER))
        .and()
        .authorizeRequests()
        	.antMatchers("/css/**", "/js/**","/error/**","/api/**")
        		.permitAll()
           	.antMatchers(API_ANT_MATCHER)
           		.hasAnyAuthority("READ")
        .and()
        	.formLogin()
        .and()
    		.logout()
        .and()
	        .authorizeRequests()
		        .anyRequest()
		    		.authenticated();
    }
    
    private class ApiEntryPoint implements AuthenticationEntryPoint {

    	@Override
    	public void commence(HttpServletRequest request, HttpServletResponse response,
    			AuthenticationException authException) throws IOException, ServletException {
    		
    		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getLocalizedMessage());
    	}

    }
}
